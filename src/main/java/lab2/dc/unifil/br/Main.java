package lab2.dc.unifil.br;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        // Realiza benchmarkings
        List<Medicao> medicoesBogosort = Cronometro.benchmarkCrescimentoAlgoritmo(
                1, 10, 1, 10,
                FabricaListas::fabricarListaIntegersAleatoria,
                Classificadores::bogosort
        );

        // Plotta gráfico com resultados levantados
        TabelaTempos tt = new TabelaTempos();
        tt.setTitulo("Tempo para ordenação");
        tt.setEtiquetaX("Qtde elementos lista");
        tt.setEtiquetaY("Tempo (s)");
        tt.setLegendas("Bubble", "Selection");
        for (int i = 0; i < medicoesBogosort.size(); i++) {
            Medicao amostraBogosort = medicoesBogosort.get(i);

            tt.anotarAmostra(amostraBogosort.getN(),
                    amostraBogosort.getTempoSegundos());
        }
        tt.exibirGraficoXY();
    }


}
